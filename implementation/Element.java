package data_structures.implementation;

public class Element<T extends Comparable<T>> {

	T data;
	Element<T> next = null;

	Element(T t) {

		this.data = t;

	}
}
