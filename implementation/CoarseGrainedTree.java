package data_structures.implementation;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;

import com.sun.net.httpserver.Authenticator.Success;

import data_structures.Sorted;

public class CoarseGrainedTree<T extends Comparable<T>> implements Sorted<T> {

	ArrayList<T> result;
	TreeNode<T> root = null;
	Lock lock;

	public void add(T t) {

		lock.lock();

		try {
			TreeNode<T> current = root;

			while (current != null) {
				if (t.compareTo(current.data) <= 0) {
					current = current.leftNode;
				} else if (t.compareTo(current.data) > 0) {
					current = current.rightNode;
				}
				current = new TreeNode<T>(t);
			}
		} finally {
			lock.unlock();
		}

		// throw new UnsupportedOperationException();
	}

	public void remove(T t) {

		lock.lock();

		try {

			TreeNode<T> current = root;
			TreeNode<T> parent = root;
			boolean leftNode = false;

			if (root.data == null) {
				return;
			}

			while (t.compareTo(current.data) != 0 && current != null) {
				if (t.compareTo(current.data) < 0) {
					parent = current;
					current = current.leftNode;
					leftNode = true;
				}
				if (t.compareTo(current.data) > 0) {
					parent = current;
					current = current.rightNode;
					leftNode = false;
				}

			}
			if (t.compareTo(current.data) == 0) {
				current.data = null;
				if (noChildren(current)) {
					removeChildlessNode(current, parent, leftNode);
				} else if (leftChild(current)) {
					removeLeftChildNode(current, parent, leftNode);
				} else if (rightChild(current)) {
					removeRightChildNode(current, parent, leftNode);
				} else if (twoChildren(current)) {
					removeTwoChildNode(current, parent, leftNode);
				}
			}
		} finally {
			lock.unlock();
		}

		// throw new UnsupportedOperationException();
	}

	TreeNode<T> nextInOrder(TreeNode<T> input) {

		TreeNode<T> current = input.rightNode;
		TreeNode<T> inOrder = null;
		TreeNode<T> inOrderParent = null;

		while (current != null) {
			inOrderParent = inOrder;
			inOrder = current;
			current = current.leftNode;
		}

		if (current != input.rightNode) {
			inOrderParent.leftNode = inOrder.rightNode;
			inOrder.rightNode = input.rightNode;
		}

		return input;

	}

	void removeChildlessNode(TreeNode<T> current, TreeNode<T> parent, boolean leftNode) {
		if (current == root) {
			root = null;
		} else if (leftNode) {
			parent.leftNode = null;
		} else {
			parent.rightNode = null;
		}
	}

	void removeLeftChildNode(TreeNode<T> current, TreeNode<T> parent, boolean leftNode) {
		if (current == root) {
			root = root.leftNode;
		} else if (leftNode) {
			parent.leftNode = current.leftNode;
		} else {
			parent.rightNode = current.leftNode;
		}
	}

	void removeRightChildNode(TreeNode<T> current, TreeNode<T> parent, boolean leftNode) {
		if (current == root) {
			root = root.rightNode;
		} else if (leftNode) {
			parent.leftNode = current.rightNode;
		} else {
			parent.rightNode = current.rightNode;
		}
	}

	void removeTwoChildNode(TreeNode<T> current, TreeNode<T> parent, boolean leftNode) {
		TreeNode<T> nextInOrder = nextInOrder(current);
		if (current == root) {
			root = nextInOrder;
		} else if (leftNode) {
			parent.leftNode = nextInOrder;
		} else {
			parent.rightNode = nextInOrder;
		}
		nextInOrder.leftNode = current.leftNode;
	}

	void addToArray(TreeNode<T> input) {

		if (input == null) {
			return;
		}
		addToArray(input.leftNode);
		result.add(input.data);
		addToArray(input.rightNode);

	}

	boolean noChildren(TreeNode<T> input) {
		return input.leftNode == null && input.rightNode == null;
	}

	boolean leftChild(TreeNode<T> input) {
		return (input.leftNode != null && input.rightNode == null);
	}

	boolean rightChild(TreeNode<T> input) {
		return input.leftNode == null && input.rightNode != null;
	}

	boolean twoChildren(TreeNode<T> input) {
		return input.leftNode != null && input.rightNode != null;
	}

	public ArrayList<T> toArrayList() {
		result = new ArrayList<>();
		addToArray(root);
		return result;
		// throw new UnsupportedOperationException();
	}
}
