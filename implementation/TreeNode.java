package data_structures.implementation;

public class TreeNode<T extends Comparable<T>> {
	
	T data;
	TreeNode<T> leftNode = null;
	TreeNode<T> rightNode = null;
	
	TreeNode(T t){
		this.data = t;
	}
	
}
