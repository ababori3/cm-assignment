package data_structures.implementation;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;

import data_structures.Sorted;

public class CoarseGrainedList<T extends Comparable<T>> implements Sorted<T> {

	Element<T> head;
	Lock lock;

	public void add(T t) {
		lock.lock();

		try {

			if (head == null) {
				head = new Element<T>(t);
			} else {
				Element<T> current = head;
				while (current.next != null) {
					current = current.next;
				}
				current.next = new Element<T>(t);
			}

		} finally {
			lock.unlock();
		}

		// throw new UnsupportedOperationException();
	}

	public void remove(T t) {
		lock.lock();

		try {
			if (head.next == null) {
				if (head.data.compareTo(t) == 0) {
					head = null;
				}
			}

			else if (head.data.compareTo(t) == 0) {
				head.data = null;
				head = head.next;
			} else {

				Element<T> current = head;

				while (current.next.data.compareTo(t) != 0 && current.next != null) {
					current = current.next;
				}

				if (current.next.data.compareTo(t) == 0) {
					current.next.data = null;
					current.next = current.next.next;
				}
			}
		}

		finally {
			lock.unlock();
		}

		// throw new UnsupportedOperationException();
	}

	public ArrayList<T> toArrayList() {

		ArrayList<T> result = new ArrayList<T>();
		Element<T> current = head;
		while (current.next != null) {
			result.add(current.data);
			current = current.next;
		}
		return result;

		// throw new UnsupportedOperationException();
	}
}
